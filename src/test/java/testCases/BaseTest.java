package testCases;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import static com.codeborne.selenide.Selenide.close;
import static com.codeborne.selenide.Selenide.open;

public class BaseTest {

    @BeforeMethod()
    public void start(){
        //Configuration.remote = "http://134.209.193.7:4444/wd/hub/";
        open("https://yandex.ru");
        WebDriverRunner.getWebDriver().manage().window().maximize();
    }

    @AfterMethod
    public void finish(){
        close();
    }

}
