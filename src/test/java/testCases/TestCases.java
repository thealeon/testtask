package testCases;

import org.testng.annotations.Test;
import pages.CatalogElektronika;
import pages.YandexMain;

public class TestCases extends BaseTest{


    @Test(description = "Поиск телефона, который оказался первым в списке")
    public void searchPhoneFirstInLest(){
        //Arrange
        CatalogElektronika elektronika = new CatalogElektronika();
        String namePhone;
        new YandexMain().goToYandexMarket()
        //Act
        .selectElectronik()
        .selectType("Мобильные телефоны")
        .inputFromPrice("20000")
        .chooseManufact("Apple","Samsung");
        namePhone = elektronika.getNameElementByNumber(1);
        elektronika.searchInYandex(namePhone)
        //Assert
        .checkNameSearchObject(namePhone);
    }

    @Test(description = "Поиск наушников, которые оказались первыми в списке")
    public void searchHeadphonesFirstInLest(){
        //Arrange
        CatalogElektronika elektronika = new CatalogElektronika();
        String nameHeadphones;
        new YandexMain().goToYandexMarket()
        //Act
        .selectElectronik()
        .selectType("Наушники и Bluetooth-гарнитуры")
        .inputFromPrice("5000")
        .chooseManufact("Beats");
        nameHeadphones = elektronika.getNameElementByNumber(1);
        elektronika.searchInYandex(nameHeadphones)
        //Assert
        .checkNameSearchObject(nameHeadphones);
    }


    @Test(description = "Проверить сортировку")
    public void checkSorting(){
        //Arrange
        new YandexMain().goToYandexMarket()
        //Act
        .selectElectronik()
        .selectType("Мобильные телефоны")
        .clickForSortByText("по цене")
        //Assert
        .checkSort();
    }

}
