package widget;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.$$;

public class RadioButtonGroup {

    ElementsCollection radioButton;

    public RadioButtonGroup(By locator) {
        this.radioButton = $$(locator);
    }

    public  void clickByText(String text){
        radioButton.findBy(Condition.text(text)).click();

    }

}
