package widget;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.$$;


public class Type {

    private ElementsCollection typeName;

    public Type(By locatorType) {
        this.typeName = $$(locatorType);
    }

    public void selectType(String type){
        typeName.findBy(Condition.exactText(type)).click();
    }
}
