package widget;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.$;

public class Panel {

    private SelenideElement panel;

    public Panel(By locator) {

        this.panel = $(locator);

    }

    public void waitLoad(){
        panel.shouldBe(Condition.visible).shouldNotBe(Condition.visible);
    }
}
