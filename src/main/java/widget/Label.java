package widget;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.$;

public class Label {
    SelenideElement label;

    public Label(By locator) {
        this.label = $(locator);
    }

    public String getText(){
       return label.getText();
    }

}
