package widget;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.$;

public class Link {

    SelenideElement link;

    public Link(By link) {
        this.link = $(link);
    }

    public void click(){
        link.click();
    }
}
