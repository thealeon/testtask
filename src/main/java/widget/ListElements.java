package widget;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import java.util.ArrayList;
import java.util.List;
import static com.codeborne.selenide.Selenide.$$;


public class ListElements {

    ElementsCollection list;

    public ListElements(By locator) {
        this.list = $$(locator);
    }

    public List<Integer> getIntegers(){
        List<Integer> integers = new ArrayList<>();
        for(SelenideElement se: list)
            integers.add(Integer.parseInt(se.shouldBe(Condition.visible).getText().replaceAll("\\D", "")));
        return integers;
    }

    public List<String> getStrings(){
        return list.texts();
    }

    public void clickByIndex(int index){
        list.get(index).click();
    }

}
