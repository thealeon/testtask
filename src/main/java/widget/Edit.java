package widget;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.$;

public class Edit {

    private SelenideElement edit;

    public Edit(By locator) {
        edit=$(locator);
    }

    public void inputText(String text){
        edit.setValue(text);
    }
}
