package widget;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.$$;

public class CheckBoxGroup {

    private ElementsCollection comboBoxGroup;

    public CheckBoxGroup(By locator) {
        comboBoxGroup=$$(locator);
    }

    public void clickByElements(String... elements) {
        for (String text : elements)
            comboBoxGroup.findBy(Condition.text(text)).click();
    }


}
