package utils;

import org.openqa.selenium.By;

public interface Locators {

    public interface YandexMain{
        By market = By.xpath("//*[@class=\"home-arrow__tabs\"]//*[text()='Маркет']");
    }

    public interface MainMenu{
        By electronik = By.linkText("Электроника");
        By search = By.id("header-search");
        By searchBtn = By.className("search2__button");
    }

    public interface CatalogElectronika{
        By allTypeList = By.xpath("//*[@class=\"_1YdrMWBuYy\"]/div[2]//a");
        By panelWithElements = By.xpath("//*[@class=\"preloadable__preloader preloadable__preloader_visibility_visible preloadable__paranja\"]");
        By priceFrom = By.id("glpricefrom");
        By manufacturerList = By.xpath("(//*[@class=\"_2y67xS5HuR\"])[1]/li//span");
        By listPrice = By.xpath("//*[@class=\"n-snippet-cell2__main-price\"]//*[@class=\"price\"]");
        By listName = By.className("n-snippet-cell2__title");
        By filterBlock = By.xpath("//*[@class= \"n-filter-block_pos_left i-bem\"]//a");
    }

    public interface SearchResult{
        By name = By.className("n-product-title__text-container-top");
    }

}
