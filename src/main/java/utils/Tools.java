package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.codeborne.selenide.Selenide.executeJavaScript;

public class Tools {

    public static void waitPageState(final String state, WebDriver driver, long timeout) {
        new WebDriverWait(driver, timeout).until((WebDriver d) -> executeJavaScript("return document.readyState;").toString().equals(state));
    }

}
