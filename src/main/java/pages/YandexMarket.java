package pages;

import io.qameta.allure.Step;
import static com.codeborne.selenide.Selenide.page;

public class YandexMarket extends MainMenu{


    @Step("Выбрать раздел Электроника")
    public CatalogElektronika selectElectronik(){
        electLink.click();
        return page(CatalogElektronika.class);
    }




}
