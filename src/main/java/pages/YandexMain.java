package pages;

import io.qameta.allure.Step;
import widget.Link;
import static com.codeborne.selenide.Selenide.page;
import static utils.Locators.YandexMain.market;


public class YandexMain {

    private Link marketLink = new Link(market);

    @Step("Перейти в яндекс маркет")
    public YandexMarket goToYandexMarket(){
        marketLink.click();
        return page(YandexMarket.class);
    }

}
