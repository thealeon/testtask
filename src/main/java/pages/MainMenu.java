package pages;

import io.qameta.allure.Step;
import widget.Button;
import widget.Edit;
import widget.Link;
import static com.codeborne.selenide.Selenide.page;
import static utils.Locators.MainMenu.*;

public class MainMenu {

    protected Edit searchEdit = new Edit(search);
    protected Link electLink = new Link(electronik);
    protected Button searchButton = new Button(searchBtn);

    @Step("Найти в Яндекс")
    public SearchResult searchInYandex(String query){
        searchEdit.inputText(query);
        searchButton.click();
        return page(SearchResult.class);
    }




}
