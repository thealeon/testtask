package pages;

import io.qameta.allure.Step;
import widget.Label;

import static utils.Locators.CatalogElectronika.listName;
import static utils.Locators.SearchResult.*;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import widget.ListElements;

public class SearchResult {

    private Label nameLabel = new Label(name);
    private ListElements listElementsName = new ListElements(listName);

    @Step("Проверить имя найденого объекта")
    public void checkNameSearchObject(String name){
        MatcherAssert.assertThat(name, Matchers.equalTo(nameLabel.getText()));
    }

    @Step("Кликнуть на товар по его порядковому номеру")
    public SearchResult clickElenentByIndex(int index){
        listElementsName.clickByIndex(index-1);
        return this;
    }



}
