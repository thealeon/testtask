package pages;

import com.google.common.collect.Ordering;
import io.qameta.allure.Step;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import widget.*;
import static utils.Locators.CatalogElectronika.*;

public class CatalogElektronika extends MainMenu{

    private Type allType = new Type(allTypeList);
    private RadioButtonGroup sortRadioButtonGroup = new RadioButtonGroup(filterBlock);
    private Panel elementsPanel = new Panel(panelWithElements);
    private ListElements listElementsPrice = new ListElements(listPrice);
    private ListElements listElementsName = new ListElements(listName);
    private Edit priceFromEdit = new Edit(priceFrom);
    private CheckBoxGroup manufacturerListCheckBoxGroup = new CheckBoxGroup(manufacturerList);


    @Step("Выбрать раздел")
    public CatalogElektronika selectType(String nameType){
        allType.selectType(nameType);
        return this;
    }

    @Step("Кликнуть для сортировки")
    public CatalogElektronika clickForSortByText(String sortName){
        sortRadioButtonGroup.clickByText(sortName);
        elementsPanel.waitLoad();//тут я ожидаю, что после клика на сортировку произойдет загрузка элементов
        return this;
    }

    @Step("Проверить сортировку")
    public CatalogElektronika checkSort(){
        MatcherAssert.assertThat(Ordering.natural().isOrdered(listElementsPrice.getIntegers()), Matchers.equalTo(true));
        // Ordering.natural().isOrdered - позволяет отсортировать от меньшего к большему
        return this;
    }

    @Step("Получить название первого элемента в списке")
    public String getNameElementByNumber(int numberElement){
        return listElementsName.getStrings().get(numberElement-1);
    }


    @Step("Ввести стоимость от которой будет происходить сортировка")
    public CatalogElektronika inputFromPrice(String price){
        priceFromEdit.inputText(price);
        elementsPanel.waitLoad();//тут я ожидаю, что после клика на сортировку произойдет загрузка элементов
        return this;
    }

    @Step("Выбрать производителей")
    public CatalogElektronika chooseManufact(String... manuf){
        manufacturerListCheckBoxGroup.clickByElements(manuf);
        elementsPanel.waitLoad();//тут я ожидаю, что после клика на сортировку произойдет загрузка элементов
        return this;
    }

}
